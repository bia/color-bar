package plugins.stef.tools.overlay;

import icy.plugin.abstract_.PluginActionable;
import icy.sequence.Sequence;

/**
 * Show a configurable color bar overlay over the active sequence.
 * 
 * @author Stephane
 */
public class ColorBar extends PluginActionable
{
    @Override
    public void run()
    {
        final Sequence seq = getActiveSequence();

        if (seq != null)
            seq.addOverlay(new ColorBarOverlay(getPreferencesRoot()));
    }
}
