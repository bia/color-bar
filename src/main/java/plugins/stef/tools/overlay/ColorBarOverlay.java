/**
 * 
 */
package plugins.stef.tools.overlay;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyChangeEvent;

import javax.swing.JPanel;

import icy.canvas.IcyCanvas;
import icy.canvas.IcyCanvas2D;
import icy.gui.util.FontUtil;
import icy.image.colormap.IcyColorMap;
import icy.image.lut.LUT;
import icy.image.lut.LUT.LUTChannel;
import icy.math.MathUtil;
import icy.painter.Overlay;
import icy.preferences.GeneralPreferences;
import icy.preferences.XMLPreferences;
import icy.sequence.Sequence;
import icy.system.thread.ThreadUtil;
import icy.util.GraphicsUtil;
import icy.util.StringUtil;
import plugins.stef.tools.overlay.ColorBarSettingPanel.SettingChangeListener;

/**
 * ColorBar overlay: display a configurable color bar.
 * 
 * @author Stephane
 */
public class ColorBarOverlay extends Overlay implements SettingChangeListener
{
    /**
     * Color bar position type
     */
    public enum ColorBarPosition
    {
        TOP, RIGHT, BOTTOM, LEFT
    }

    protected ColorBarSettingPanel settingPanel;
    protected final XMLPreferences prefs;

    /**
     * Create a new color bar Overlay with preferences stores in specified XML node.
     * 
     * @param prefs
     *        the preferences object
     */
    public ColorBarOverlay(XMLPreferences prefs)
    {
        super("Color bar", OverlayPriority.TEXT_LOW);

        this.prefs = prefs;

        ThreadUtil.invokeNow(new Runnable()
        {
            @Override
            public void run()
            {
                settingPanel = new ColorBarSettingPanel();

                // load preferences
                if (prefs != null)
                {
                    settingPanel.setColorBarOffsetX(prefs.getDouble(ColorBarSettingPanel.PROPERTY_OFFSET_X, 0d));
                    settingPanel.setColorBarOffsetY(prefs.getDouble(ColorBarSettingPanel.PROPERTY_OFFSET_Y, 0d));
                    settingPanel.setColorBarScale(prefs.getDouble(ColorBarSettingPanel.PROPERTY_SCALE, 1d));
                    settingPanel.setPosition(ColorBarPosition.values()[prefs
                            .getInt(ColorBarSettingPanel.PROPERTY_POSITION, ColorBarPosition.RIGHT.ordinal())]);
                    settingPanel
                            .setDisplayMinMax(prefs.getBoolean(ColorBarSettingPanel.PROPERTY_DISPLAY_MINMAX, false));
                }
            }
        });

        // listen setting change
        settingPanel.addSettingChangeListener(ColorBarOverlay.this);
    }

    @Override
    public JPanel getOptionsPanel()
    {
        return settingPanel;
    }

    @Override
    public void paint(Graphics2D g, Sequence sequence, IcyCanvas canvas)
    {
        // Unnecessary (canvas != null) condition
        if ((g != null) && (canvas instanceof IcyCanvas2D))
        {
            final LUT lut = canvas.getLut();

            if (lut != null)
            {
                final double offsetXd = getColorBarOffsetX();
                final double offsetYd = getColorBarOffsetY();
                final double scale = getColorBarScale();
                final ColorBarPosition position = getPosition();
                final boolean displayMinMax = getDisplayMinMax();

                final IcyCanvas2D cnv2d = (IcyCanvas2D) canvas;
                final Graphics2D g2 = (Graphics2D) g.create();
                final int numChannel = lut.getNumChannel();
                final Rectangle region;
                double x, y;
                double w, h;

                // get back to canvas coordinate
                g2.transform(cnv2d.getInverseTransform());
                region = new Rectangle(0, 0, cnv2d.getCanvasSizeX(), cnv2d.getCanvasSizeY());// g2.getClipBounds();

                final int offsetX = (int) ((cnv2d.getCanvasSizeX() / 2d) * offsetXd);
                final int offsetY = (int) ((cnv2d.getCanvasSizeY() / 2d) * offsetYd);

                switch (position)
                {
                    case BOTTOM:
                    case TOP:
                        w = Math.max(region.width / 2, 80);
                        h = Math.min(Math.max(region.height / 20, 10), 48);
                        w *= scale;
                        h *= scale;

                        x = region.x;
                        x += (region.width / 2d) - (w / 2);
                        x += offsetX;
                        if (position == ColorBarPosition.BOTTOM)
                        {
                            y = region.y + region.height;
                            y -= h * (1 + numChannel);
                        }
                        else
                        {
                            y = region.y;
                            y += h;
                        }
                        y += offsetY;
                        break;

                    default:
                    case LEFT:
                    case RIGHT:
                        w = Math.min(Math.max(region.width / 20, 10), 48);
                        h = Math.max(region.height / 2, 80);
                        w *= scale;
                        h *= scale;

                        if (position == ColorBarPosition.RIGHT)
                        {
                            x = region.x + region.width;
                            x -= w * (1 + numChannel);
                        }
                        else
                        {
                            x = region.x;
                            x += w;
                        }
                        x += offsetX;
                        y = region.y;
                        y += (region.height / 2d) - (h / 2);
                        y += offsetY;
                        break;
                }

                int xi, yi;
                int wi, hi;
                final double ratio;

                xi = (int) Math.round(x);
                yi = (int) Math.round(y);
                wi = (int) Math.round(w);
                hi = (int) Math.round(h);

                switch (position)
                {
                    case BOTTOM:
                    case TOP:
                        ratio = 255d / (wi - 1);

                        for (int c = 0; c < numChannel; c++)
                        {
                            final LUTChannel lutChannel = lut.getLutChannel(c);
                            final IcyColorMap colorMap = lutChannel.getColorMap();

                            // draw white background
                            g2.setColor(Color.white);
                            g2.fillRect(xi, yi, wi + 2, hi + 1);

                            // draw colormap content
                            for (int i = 0; i < (int) w; i++)
                            {
                                final int colorInd = (int) Math.round(i * ratio);
                                g2.setColor(colorMap.getColor(Math.min(255, Math.max(0, colorInd))));
                                g2.fillRect(xi + 1 + i, yi + 1, 1, hi - 1);
                            }

                            if (displayMinMax)
                            {
                                String text;
                                Rectangle2D textBnd;
                                Font font;

                                g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                                        RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                                g2.setColor(colorMap.getDominantColor());
                                font = FontUtil.setSize(g2.getFont(), GeneralPreferences.getGuiFontSize());
                                g2.setFont(font);

                                // display minimum
                                text = StringUtil.toString(MathUtil.round(lut.getLutChannel(c).getMin(), 2));
                                textBnd = GraphicsUtil.getStringBounds(g2, text);
                                GraphicsUtil.drawString(g2, text, xi - (int) (textBnd.getWidth() + 8),
                                        yi + ((hi - (int) textBnd.getHeight()) / 2), true);
                                // display maximum
                                text = StringUtil.toString(MathUtil.round(lut.getLutChannel(c).getMax(), 2));
                                textBnd = GraphicsUtil.getStringBounds(g2, text);
                                GraphicsUtil.drawString(g2, text, xi + wi + 8,
                                        yi + ((hi - (int) textBnd.getHeight()) / 2), true);
                            }

                            // draw next channel
                            yi += hi;
                        }
                        break;

                    default:
                    case RIGHT:
                    case LEFT:
                        ratio = 255d / (hi - 1);

                        for (int c = 0; c < numChannel; c++)
                        {
                            final IcyColorMap colorMap = lut.getLutChannel(c).getColorMap();

                            // draw white background
                            g2.setColor(Color.white);
                            g2.fillRect(xi, yi, wi + 1, hi + 2);

                            // draw colormap content
                            for (int i = 0; i < hi; i++)
                            {
                                final int colorInd = (int) Math.round(((hi - 1) - i) * ratio);
                                g2.setColor(colorMap.getColor(Math.min(255, Math.max(0, colorInd))));
                                g2.fillRect(xi + 1, yi + 1 + i, wi - 1, 1);
                            }

                            if (displayMinMax)
                            {
                                String text;
                                Rectangle2D textBnd;
                                Font font;

                                g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                                        RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                                g2.setColor(colorMap.getDominantColor());
                                font = FontUtil.setSize(g2.getFont(), GeneralPreferences.getGuiFontSize());
                                g2.setFont(font);

                                // display minimum
                                text = StringUtil.toString(MathUtil.round(lut.getLutChannel(c).getMin(), 2));
                                textBnd = GraphicsUtil.getStringBounds(g2, text);
                                GraphicsUtil.drawString(g2, text, xi + ((wi - (int) textBnd.getWidth()) / 2),
                                        yi + hi + 5, true);
                                // display maximum
                                text = StringUtil.toString(MathUtil.round(lut.getLutChannel(c).getMax(), 2));
                                textBnd = GraphicsUtil.getStringBounds(g2, text);
                                GraphicsUtil.drawString(g2, text, xi + ((wi - (int) textBnd.getWidth()) / 2),
                                        yi - (int) (textBnd.getHeight() + 4), true);
                            }

                            // next channel
                            xi += wi;
                        }
                        break;
                }

                g2.dispose();
            }
        }
    }

    /**
     * @return the display min/max state
     */
    public boolean getDisplayMinMax()
    {
        return settingPanel.getDisplayMinMax();
    }

    /**
     * Sets the display min/max state (default = true)
     * 
     * @param value
     *        the value to set
     */
    public void setDisplayMinMax(boolean value)
    {
        settingPanel.setDisplayMinMax(value);
    }

    /**
     * @return the offset X of the color bar (from -1d to 1d)
     */
    public double getColorBarOffsetX()
    {
        return settingPanel.getColorBarOffsetX();
    }

    /**
     * Sets the offset X of the color bar (default = 0d)
     * 
     * @param value
     *        the value to set
     */
    public void setColorBarOffsetX(double value)
    {
        settingPanel.setColorBarOffsetX(value);
    }

    /**
     * @return the offset Y of the color bar (from -1d to 1d)
     */
    public double getColorBarOffsetY()
    {
        return settingPanel.getColorBarOffsetY();
    }

    /**
     * Sets the offset Y of the color bar (default = 0d)
     * 
     * @param value
     *        the value to set
     */
    public void setColorBarOffsetY(double value)
    {
        settingPanel.setColorBarOffsetY(value);
    }

    /**
     * @return the scale of the color bar
     */
    public double getColorBarScale()
    {
        return settingPanel.getColorBarScale();
    }

    /**
     * Sets the scale of the color bar (default = 1d)
     * 
     * @param value
     *        the value to set
     */
    public void setColorBarScale(double value)
    {
        settingPanel.setColorBarScale(value);
    }

    /**
     * @return the position of the color bar.<br>
     *         Possible values are:<br>
     *         <ul>
     *         <li>{@link ColorBarPosition#LEFT}</li>
     *         <li>{@link ColorBarPosition#TOP}</li>
     *         <li>{@link ColorBarPosition#RIGHT} (default)</li>
     *         <li>{@link ColorBarPosition#BOTTOM}</li>
     *         </ul>
     */
    public ColorBarPosition getPosition()
    {
        return settingPanel.getPosition();
    }

    /**
     * Sets the position of the color bar.<br>
     * Possible values are:<br>
     * <ul>
     * <li>{@link ColorBarPosition#LEFT}</li>
     * <li>{@link ColorBarPosition#TOP}</li>
     * <li>{@link ColorBarPosition#RIGHT} (default)</li>
     * <li>{@link ColorBarPosition#BOTTOM}</li>
     * </ul>
     * 
     * @param value
     *        the value to set
     */
    public void setPosition(ColorBarPosition value)
    {
        settingPanel.setPosition(value);
    }

    @Override
    public void settingChange(PropertyChangeEvent event)
    {
        // save preferences
        if (prefs != null)
        {
            final String propertyName = event.getPropertyName();
            final Object value = event.getNewValue();

            // More readable than "if-else-if" block
            switch (propertyName) {
                case ColorBarSettingPanel.PROPERTY_DISPLAY_MINMAX:
                    prefs.putBoolean(ColorBarSettingPanel.PROPERTY_DISPLAY_MINMAX, ((Boolean) value).booleanValue());
                    break;
                case ColorBarSettingPanel.PROPERTY_OFFSET_X:
                    prefs.putDouble(ColorBarSettingPanel.PROPERTY_OFFSET_X, ((Double) value).doubleValue());
                    break;
                case ColorBarSettingPanel.PROPERTY_OFFSET_Y:
                    prefs.putDouble(ColorBarSettingPanel.PROPERTY_OFFSET_Y, ((Double) value).doubleValue());
                    break;
                case ColorBarSettingPanel.PROPERTY_POSITION:
                    prefs.putInt(ColorBarSettingPanel.PROPERTY_POSITION, ((ColorBarPosition) value).ordinal());
                    break;
                case ColorBarSettingPanel.PROPERTY_SCALE:
                    prefs.putDouble(ColorBarSettingPanel.PROPERTY_SCALE, ((Double) value).doubleValue());
                    break;
            }
        }

        // redraw the colorbar
        painterChanged();
    }
}
