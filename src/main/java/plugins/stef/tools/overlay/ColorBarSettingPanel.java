package plugins.stef.tools.overlay;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.util.EventListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import icy.gui.dialog.ConfirmDialog;
import plugins.stef.tools.overlay.ColorBarOverlay.ColorBarPosition;

public class ColorBarSettingPanel extends JPanel implements ActionListener, ChangeListener
{
    public interface SettingChangeListener extends EventListener
    {
        void settingChange(PropertyChangeEvent event);
    }

    public final static String PROPERTY_POSITION = "position";
    public final static String PROPERTY_SCALE = "scale";
    public final static String PROPERTY_OFFSET_X = "offsetX";
    public final static String PROPERTY_OFFSET_Y = "offsetY";
    public final static String PROPERTY_DISPLAY_MINMAX = "displayMinMax";

    protected JComboBox<ColorBarPosition> positionComboBox;
    protected JSlider offsetXSlider;
    protected JSlider offsetYSlider;
    protected JSlider scaleSlider;
    protected JCheckBox minMaxCheckBox;
    protected JButton resetDefaultButton;

    /**
     * Create the panel.
     */
    public ColorBarSettingPanel()
    {
        super();

        initialize();

        positionComboBox.setSelectedItem(ColorBarPosition.RIGHT);
        offsetXSlider.setValue(0);
        offsetYSlider.setValue(0);
        scaleSlider.setValue(100);
        minMaxCheckBox.setSelected(true);

        positionComboBox.addActionListener(this);
        scaleSlider.addChangeListener(this);
        offsetXSlider.addChangeListener(this);
        offsetYSlider.addChangeListener(this);
        minMaxCheckBox.addChangeListener(this);
        resetDefaultButton.addActionListener(this);
    }

    public void addSettingChangeListener(SettingChangeListener listener)
    {
        listenerList.add(SettingChangeListener.class, listener);
    }

    public void removeSettingChangeListener(SettingChangeListener listener)
    {
        listenerList.remove(SettingChangeListener.class, listener);
    }

    private void initialize()
    {
        setBorder(new EmptyBorder(2, 0, 0, 0));

        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[] {120, 40, 0, 0};
        gridBagLayout.rowHeights = new int[] {0, 0, 0, 0, 0, 0};
        gridBagLayout.columnWeights = new double[] {0.0, 0.0, 1.0, Double.MIN_VALUE};
        gridBagLayout.rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        setLayout(gridBagLayout);

        JLabel lblPosition = new JLabel(" Position");
        GridBagConstraints gbc_lblPosition = new GridBagConstraints();
        gbc_lblPosition.insets = new Insets(0, 0, 5, 5);
        gbc_lblPosition.anchor = GridBagConstraints.WEST;
        gbc_lblPosition.gridx = 0;
        gbc_lblPosition.gridy = 0;
        add(lblPosition, gbc_lblPosition);

        positionComboBox = new JComboBox<>();
        positionComboBox.setToolTipText("Position of the color bar");
        positionComboBox.setModel(new DefaultComboBoxModel<>(ColorBarPosition.values()));
        GridBagConstraints gbc_positionComboBox = new GridBagConstraints();
        gbc_positionComboBox.fill = GridBagConstraints.HORIZONTAL;
        gbc_positionComboBox.gridwidth = 2;
        gbc_positionComboBox.insets = new Insets(0, 0, 5, 0);
        gbc_positionComboBox.gridx = 1;
        gbc_positionComboBox.gridy = 0;
        add(positionComboBox, gbc_positionComboBox);

        JLabel lblOffset = new JLabel(" Offset X");
        GridBagConstraints gbc_lblOffset = new GridBagConstraints();
        gbc_lblOffset.anchor = GridBagConstraints.WEST;
        gbc_lblOffset.insets = new Insets(0, 0, 5, 5);
        gbc_lblOffset.gridx = 0;
        gbc_lblOffset.gridy = 1;
        add(lblOffset, gbc_lblOffset);

        offsetXSlider = new JSlider();
        offsetXSlider.setPreferredSize(new Dimension(60, 26));
        offsetXSlider.setToolTipText("X position offset of the color bar");
        offsetXSlider.setMinorTickSpacing(10);
        offsetXSlider.setMajorTickSpacing(50);
        offsetXSlider.setPaintTicks(true);
        offsetXSlider.setMinimum(-100);
        GridBagConstraints gbc_offsetXSlider = new GridBagConstraints();
        gbc_offsetXSlider.fill = GridBagConstraints.HORIZONTAL;
        gbc_offsetXSlider.gridwidth = 2;
        gbc_offsetXSlider.insets = new Insets(0, 0, 5, 0);
        gbc_offsetXSlider.gridx = 1;
        gbc_offsetXSlider.gridy = 1;
        add(offsetXSlider, gbc_offsetXSlider);

        JLabel lblOffsetY = new JLabel(" Offset Y");
        GridBagConstraints gbc_lblOffsetY = new GridBagConstraints();
        gbc_lblOffsetY.anchor = GridBagConstraints.WEST;
        gbc_lblOffsetY.insets = new Insets(0, 0, 5, 5);
        gbc_lblOffsetY.gridx = 0;
        gbc_lblOffsetY.gridy = 2;
        add(lblOffsetY, gbc_lblOffsetY);

        offsetYSlider = new JSlider();
        offsetYSlider.setPreferredSize(new Dimension(60, 26));
        offsetYSlider.setToolTipText("Y position offset of the color bar");
        offsetYSlider.setMinorTickSpacing(10);
        offsetYSlider.setMajorTickSpacing(50);
        offsetYSlider.setPaintTicks(true);
        offsetYSlider.setMinimum(-100);
        GridBagConstraints gbc_offsetYSlider = new GridBagConstraints();
        gbc_offsetYSlider.fill = GridBagConstraints.HORIZONTAL;
        gbc_offsetYSlider.gridwidth = 2;
        gbc_offsetYSlider.insets = new Insets(0, 0, 5, 0);
        gbc_offsetYSlider.gridx = 1;
        gbc_offsetYSlider.gridy = 2;
        add(offsetYSlider, gbc_offsetYSlider);

        JLabel lblWidth = new JLabel(" Scale");
        GridBagConstraints gbc_lblWidth = new GridBagConstraints();
        gbc_lblWidth.anchor = GridBagConstraints.WEST;
        gbc_lblWidth.insets = new Insets(0, 0, 5, 5);
        gbc_lblWidth.gridx = 0;
        gbc_lblWidth.gridy = 3;
        add(lblWidth, gbc_lblWidth);

        scaleSlider = new JSlider();
        scaleSlider.setPreferredSize(new Dimension(60, 26));
        scaleSlider.setToolTipText("Scale factor of the color bar");
        scaleSlider.setMinorTickSpacing(10);
        scaleSlider.setMajorTickSpacing(50);
        scaleSlider.setMaximum(200);
        scaleSlider.setPaintTicks(true);
        scaleSlider.setMinimum(50);
        GridBagConstraints gbc_sizeSlider = new GridBagConstraints();
        gbc_sizeSlider.fill = GridBagConstraints.HORIZONTAL;
        gbc_sizeSlider.gridwidth = 2;
        gbc_sizeSlider.insets = new Insets(0, 0, 5, 0);
        gbc_sizeSlider.gridx = 1;
        gbc_sizeSlider.gridy = 3;
        add(scaleSlider, gbc_sizeSlider);

        JLabel lblDisplayMin = new JLabel(" Display min / max");
        GridBagConstraints gbc_lblDisplayMin = new GridBagConstraints();
        gbc_lblDisplayMin.anchor = GridBagConstraints.WEST;
        gbc_lblDisplayMin.insets = new Insets(0, 0, 0, 5);
        gbc_lblDisplayMin.gridx = 0;
        gbc_lblDisplayMin.gridy = 4;
        add(lblDisplayMin, gbc_lblDisplayMin);

        minMaxCheckBox = new JCheckBox("");
        minMaxCheckBox.setToolTipText("Display the minimum and maximum intensity value");
        GridBagConstraints gbc_minMaxCheckBox = new GridBagConstraints();
        gbc_minMaxCheckBox.anchor = GridBagConstraints.WEST;
        gbc_minMaxCheckBox.insets = new Insets(0, 0, 0, 5);
        gbc_minMaxCheckBox.gridx = 1;
        gbc_minMaxCheckBox.gridy = 4;
        add(minMaxCheckBox, gbc_minMaxCheckBox);

        resetDefaultButton = new JButton("Restore default");
        resetDefaultButton.setToolTipText("Restore default values");
        GridBagConstraints gbc_resetDefaultButton = new GridBagConstraints();
        gbc_resetDefaultButton.fill = GridBagConstraints.HORIZONTAL;
        gbc_resetDefaultButton.gridx = 2;
        gbc_resetDefaultButton.gridy = 4;
        add(resetDefaultButton, gbc_resetDefaultButton);

        validate();
    }

    /**
     * @return the display min/max state
     */
    public boolean getDisplayMinMax()
    {
        return minMaxCheckBox.isSelected();
    }

    /**
     * Sets the display min/max state (default = true)
     * 
     * @param value
     *        the value to set
     */
    public void setDisplayMinMax(boolean value)
    {
        minMaxCheckBox.setSelected(value);
    }

    /**
     * @return the offset X of the color bar (from -1d to 1d)
     */
    public double getColorBarOffsetX()
    {
        return offsetXSlider.getValue() / 100d;
    }

    /**
     * Sets the offset X of the color bar (default = 0d)
     * 
     * @param value
     *        the value to set
     */
    public void setColorBarOffsetX(double value)
    {
        offsetXSlider.setValue((int) (value * 100d));
    }

    /**
     * @return the offset Y of the color bar (from -1d to 1d)
     */
    public double getColorBarOffsetY()
    {
        return offsetYSlider.getValue() / 100d;
    }

    /**
     * Sets the offset Y of the color bar (default = 0d)
     * 
     * @param value
     *        the value to set
     */
    public void setColorBarOffsetY(double value)
    {
        offsetYSlider.setValue((int) (value * 100d));
    }

    /**
     * @return the scale of the color bar
     */
    public double getColorBarScale()
    {
        return scaleSlider.getValue() / 100d;
    }

    /**
     * Sets the scale of the color bar (default = 1d)
     * 
     * @param value
     *        the value to set
     */
    public void setColorBarScale(double value)
    {
        scaleSlider.setValue((int) (value * 100d));
    }

    /**
     * @return the position of the color bar.<br>
     *         Possible values are:<br>
     *         <ul>
     *         <li>{@link ColorBarPosition#LEFT}</li>
     *         <li>{@link ColorBarPosition#TOP}</li>
     *         <li>{@link ColorBarPosition#RIGHT} (default)</li>
     *         <li>{@link ColorBarPosition#BOTTOM}</li>
     *         </ul>
     */
    public ColorBarPosition getPosition()
    {
        return (ColorBarPosition) positionComboBox.getSelectedItem();
    }

    /**
     * Sets the position of the color bar.<br>
     * Possible values are:<br>
     * <ul>
     * <li>{@link ColorBarPosition#LEFT}</li>
     * <li>{@link ColorBarPosition#TOP}</li>
     * <li>{@link ColorBarPosition#RIGHT} (default)</li>
     * <li>{@link ColorBarPosition#BOTTOM}</li>
     * </ul>
     * 
     * @param value
     *        the value to set
     */
    public void setPosition(ColorBarPosition value)
    {
        positionComboBox.setSelectedItem(value);
    }

    public void fireSettingChanged(Object source, String property, Object value)
    {
        final PropertyChangeEvent event = new PropertyChangeEvent(source, property, null, value);

        for (SettingChangeListener listener : getListeners(SettingChangeListener.class))
            listener.settingChange(event);
    }

    @Override
    public void stateChanged(ChangeEvent e)
    {
        final Object source = e.getSource();

        if (source == scaleSlider)
            fireSettingChanged(source, PROPERTY_SCALE, Double.valueOf(getColorBarScale()));
        else if (source == offsetXSlider)
            fireSettingChanged(source, PROPERTY_OFFSET_X, Double.valueOf(getColorBarOffsetX()));
        else if (source == offsetYSlider)
            fireSettingChanged(source, PROPERTY_OFFSET_Y, Double.valueOf(getColorBarOffsetY()));
        else if (source == minMaxCheckBox)
            fireSettingChanged(source, PROPERTY_DISPLAY_MINMAX, Boolean.valueOf(minMaxCheckBox.isSelected()));
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        final Object source = e.getSource();

        if (source == positionComboBox)
            fireSettingChanged(source, PROPERTY_POSITION, positionComboBox.getSelectedItem());
        else if (source == resetDefaultButton)
        {
            if (ConfirmDialog.confirm("Restore defaults settings of color bar ?"))
            {
                setDisplayMinMax(false);
                setColorBarOffsetX(0d);
                setColorBarOffsetY(0d);
                setColorBarScale(1d);
                setPosition(ColorBarPosition.RIGHT);
            }
        }
    }
}
